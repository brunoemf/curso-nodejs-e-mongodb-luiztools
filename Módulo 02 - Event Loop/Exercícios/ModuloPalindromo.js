function verificarPalindromo(palavra){
    var contrario = "";
    // Fazer um for ao contrario se a palava montada for igual a palavra informada então é palindromo
    for( var i = palavra.length -1; i >= 0; i--)
       contrario += palavra[i];
    
    return palavra === contrario;
}

module.exports = {verificarPalindromo}