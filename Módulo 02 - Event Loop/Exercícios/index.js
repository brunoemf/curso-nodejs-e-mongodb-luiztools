var ModuloCalculadora = require("./ModuloCalculadora");
var ModuloParouImpar = require("./ModuloParouImpar");
var ModuloPalindromo = require("./ModuloPalindromo");

// Executando a Calculadora
console.log(" ******* Executa Módulo Calculadora *******");
console.log( "Soma = " + ModuloCalculadora.somar(1,2) );
console.log( "Subtração = " + ModuloCalculadora.subtrair(2,2) );
console.log( "Multiplicação = " + ModuloCalculadora.multiplicar(1,2) );
console.log( "Divisão = " + ModuloCalculadora.dividir(1,2) );

console.log(" ******* Executa Módulo Par ou Impar *******");
ModuloParouImpar.verificarParOuImpar(9);

console.log(" ******* Executa Módulo Palindromo *******");
var palavra = "Texto";
if ( ModuloPalindromo.verificarPalindromo(palavra) === true ) {
    console.log("Palavra " + palavra + " é Palindromo !");
 } else 
    console.log("Palavra " + palavra + " não é Palindromo !");