function verificarParOuImpar(numero) {
    // Quando dividimos um numero por 2 e o resto da divisão é zero quer dizer que esse numero é par
    if(numero % 2 === 0) 
       console.log("Este numero é Par !");
     else 
       console.log("Este numero é Ímpar !");  
} 
       

module.exports = {verificarParOuImpar}