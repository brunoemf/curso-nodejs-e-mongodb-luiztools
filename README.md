# Curso NodeJS e MongoDB LuizTools

Arquivos, Fontes, Documentação e Anotações do Curso do LuizTools de NodeJs e MongoDB.

# Módulo 1
* Cronograma 
Por que Node JS ? 
  * JavasCript
     * Linguagem Web mais popular do Mundo;
     * Baixa Curva de Aprendizado;
     * Comunidade Forte;
  * Full-Stack
     * Javascript permite ser FullStack;
     * Javascript client-side;
     * Javascript server-side( Node.JS )
     * Javascript database( MongoDB )
     * Dados Json em todas as camadas;
  * Node.JS ( Node.JS é Um interpretador Javascript; )
     * baixo custo de hardware;
     * baixo custo de SO pode ser usado em qualquer SO;
     * sem licença; 
     * Tecnologia sendo usada em escala corporativa;
        * Netflix / Microsoft;
        * GoDaddy / Groupon;
        * Paypal / Rakuten;
        * SAP / Walmart;
        * Yahoo / Ebay;
     * Ferramentas : 
        * VS Code;
        * Atom;
        * NetBeans
     * Ambiente :
        * Node.JS é Um interpretador Javascript;     
        * Node é uma plataforma para aplicações 
        * é composta por um motor chamado V8;
        * Biblioteca IO ( libuv biblioteca C++ )
        * package Manager ( NPM ) Gerenciador de Pacotes;
        * Ferramenta REPL ( utilização de console por linha de comando );
        * Version Manager ( NVM ) consegue chavear versões diferentes do node na mesma maquina; 
  * MongoDB
    * Não Relacional;
    * BSON ( Json Binário )
    * Não possui instalação apenas extração de uma pasta;

# Módulo 2
* Node.JS
   * Criado por Ryan Dahl ( enquanto trabalhava na Joyent );
   * Características :
      * Inerpretador (V8) é Single Thread + libuv é MultiThread
      * Single Thread 
        * Event Loop : Quando chega uma requisição para o Node essa requisição vai para uma fila de   eventos, caso ela for a primeira da fila, ela vai entrar no event Loop e o Event Loop vai verifica se é rápido processar esse evento, caso seja rápido processar o evento o event loop processa esse evento e devolve para o usuário e assim vem o próximo evento ele verifica novamente se ele verificar que é mais demorado processar esse evento ele envia esse evento para um pool de Threads em Background, e enquanto esse evento esta sendo processado estão chegando mais eventos e nesse tempo ele continua processando os eventos que estão chegando, e logo que acabar de ser processado o evento pelo pool de thread ele volta para a fila de eventos para ser tratado novamente pelo Event Loop.  
      * Assíncrono
      * = Plataforma != Linguagem != Framework
      * Node é um hospedeiro de Aplicações = Plataforma
      * Ótimo para criar API´s ( 80% da utilização do node é para criação de apis )
      * BackEnd de Games
      * IOT
      * Mensageria
    * Cada arquivo .Js para o# Módulo 2v node é um módulo
      
# Módulo 3
* ExpressJS ( Framework Web )
  * Instalação do ExpressJS-generator ( Scaffold de Express cria templates de projeto Express )
  $ npm install -g express-generator 
* EJS ( Motor de Renderização )
  * Embedded JavaScript
  * HTML + JS Server Side( Node.Js )
  * Renderiza HTML
  * Server Tag <%= %>
  * Model
  * forEach
  * Observações : 
    * Comando para criação dos projetos : 
      * Cria o projeto com suporte a EJS e cria o arquivo .gitignore : $ express -e --git cadastro-fake-produtos
      * cria as dependencias do pojeto : $ npm install
      * sobe o servidor : $ npm start  
     
    * Os exercícios estão com arquivo .gitignore ou seja é necessário carregar as dependencias neles novamente utilize o comando : $ npm install 
    * Depois para subir o Servidor utilize :  $ npm start

# Módulo 4
