var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  // se global.carros não houver nada ele passa um array vazio
  if(!global.carros) global.carros = [];
  res.render('index', { title: 'Express', carros:global.carros });
});

module.exports = router;
