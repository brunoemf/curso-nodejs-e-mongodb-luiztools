var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('cadastrolivros',{} );
});

/* Post home page. */
router.post('/', function(req, res, next) {
    global.livros.push({editora:req.body.editora,
                        nome:req.body.nome,
                        ano:req.body.ano})
    res.redirect('/');
  });

module.exports = router;
