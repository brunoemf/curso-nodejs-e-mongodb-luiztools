var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  // se global.livros não houver nada ele passa um array vazio
  if(!global.livros) global.livros = [];
  res.render('index', { title: 'Express', livros:global.livros });
});

module.exports = router;
