var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('cadastroprodutos',{} );
});

/* Post home page. */
router.post('/', function(req, res, next) {
    global.produtos.push({tipo:req.body.tipo,
                        descricao:req.body.descricao,
                        valor:req.body.valor})
    res.redirect('/');
  });

module.exports = router;
