var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  // se global.produtos não houver nada ele passa um array vazio
  if(!global.produtos) global.produtos = [];
  res.render('index', { title: 'Express', produtos:global.produtos });
});

module.exports = router;
